import React, {Component} from 'react';
import {StyleSheet, FlatList, Text, View, Button, Modal, TouchableHighlight, Image} from 'react-native';
import {connect} from 'react-redux';

import ListItem from '../../components/ListItem/ListItem';
import {getList} from "../../store/action";

class List extends Component {
    state = {
        modalVisible: false,
        clickedContact: null
    };

    componentDidMount() {
        this.props.getList();
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    showInformation = contact => {
        this.setState({clickedContact: contact});
        this.setModalVisible(true);
    };

    render() {
        const clickedContact = {...this.state.clickedContact};

        if (this.props.loading) {
            return (
                <View style={styles.loading}>
                    <Text>Loading...</Text>
                </View>
            )
        } else {
            return (
                <View style={styles.container}>
                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                        <View style={{marginTop: 22, paddingHorizontal: 20}}>
                            <View style={styles.modalContent}>
                                <Text style={{fontSize: 30, marginBottom: 10}}>{clickedContact.name}</Text>
                                <Image style={{width: 150, height: 150, marginBottom: 10}}
                                       source={{uri: clickedContact.photo}}/>
                                <Text style={{fontSize: 16, marginBottom: 10}}>Email: {clickedContact.email}</Text>
                                <Text style={{fontSize: 16, marginBottom: 10}}>Phone number: {clickedContact.phone}</Text>
                                <Button
                                    onPress={() => {this.setModalVisible(!this.state.modalVisible);}}
                                    title="Back to list"
                                    color="#841584"
                                    accessibilityLabel="Learn more about this purple button"
                                />
                            </View>
                        </View>
                    </Modal>
                    <FlatList
                        data={this.props.arrayList}
                        keyExtractor={(item) => item.id}
                        renderItem={(info) => (
                        <ListItem
                            show={() => this.showInformation(info.item)}
                            text={info.item.name}
                            img={info.item.photo}
                        />
                    )}
                    />
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        marginVertical: 30
    },
    loading: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalContent: {
        alignItems: 'center'
    }
});

const mapToProps = state => {
    return {
        arrayList: state.arrayList,
        loading: state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getList: () => dispatch(getList())
    }
};

export default connect(mapToProps, mapDispatchToProps)(List);