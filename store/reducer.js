import {LIST_SUCCESS, LIST_REQUEST, LIST_ERROR, SHOW_INFORMATION} from "./action";

const initialState = {
    arrayList: [],
    clickedContact: null,
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LIST_REQUEST:
            return {
                ...state,
                loading: true
            };
        case LIST_SUCCESS:
            const contacts = [];
            for (let key in action.listData) {
                contacts.push({...action.listData[key], id: key})
            }

            return {
                ...state,
                arrayList: contacts,
                loading: false
            };
        case LIST_ERROR:
            return {
                ...state,
                loading: false
            };
        default:
            return state;
    }
};

export default reducer;