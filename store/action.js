import axios from '../axios-contacts';

export const LIST_REQUEST = 'LIST_REQUEST';
export const LIST_SUCCESS = 'LIST_SUCCESS';
export const LIST_ERROR = 'LIST_ERROR';
export const SHOW_INFORMATION = 'SHOW_INFORMATION';

// export const showInformation = contact => {
//     return {type: SHOW_INFORMATION, contact}
// };

export const listRequest = () => {
  return {type: LIST_REQUEST}
};

export const listSuccess = (listData) => {
  return {type: LIST_SUCCESS, listData}
};

export const listError = () => {
    return {type: LIST_ERROR}
};

export const getList = () => {
    return dispatch => {
        dispatch(listRequest());
        axios.get('/contacts.json').then(response => {
            dispatch(listSuccess(response.data));
        }, error => {
            dispatch(listError());
        })
    }
};