import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

const ListItem = props => {
    return (
        <TouchableOpacity style={styles.listItem} onPress={props.show}>
            <Image style={{width: 50, height: 50, marginRight: 10}} source={{uri: props.img}}/>
            <Text style={{width: '80%'}}>{props.text}</Text>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    listItem: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#eee',
        width: '100%',
        marginBottom: 10,
        padding: 10
    },
});

export default ListItem;